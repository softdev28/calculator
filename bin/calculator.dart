import 'dart:io';

void main(List<String> args) {
  bool finish = false;
  while (!finish) {
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    print("MENU");
    print("Select the choice you want to perform :");
    print("1. ADD");
    print("2. SUBTRACT");
    print("3. MULTIPLY");
    print("4. DIVIDE");
    print("5. EXIT");
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    print("Choice you want to enter");
    int? select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        {
          print("Enter the value for x :");
          int? x = int.parse(stdin.readLineSync()!);
          print("Enter the value for y :");
          int? y = int.parse(stdin.readLineSync()!);
          print("Sum of the two numbers is :");
          print(x + y);
        }
        break;
      case 2:
        print("Enter the value for x :");
        int? x = int.parse(stdin.readLineSync()!);
        print("Enter the value for y :");
        int? y = int.parse(stdin.readLineSync()!);
        print("Different of the two numbers is :");
        print(x - y);
        break;
      case 3:
        {
          print("Enter the value for x :");
          int? x = int.parse(stdin.readLineSync()!);
          print("Enter the value for y :");
          int? y = int.parse(stdin.readLineSync()!);
          print("Product of the two numbers is :");
          print(x * y);
        }
        break;
      case 4:
        {
          print("Enter the value for x :");
          int? x = int.parse(stdin.readLineSync()!);
          print("Enter the value for y :");
          int? y = int.parse(stdin.readLineSync()!);
          print("Divide result of the two numbers is :");
          print(x / y);
        }
        break;
      case 5:
        {
          print("-----------------END-------------------");
          finish = true;
        }
        break;
    }
  }
}
